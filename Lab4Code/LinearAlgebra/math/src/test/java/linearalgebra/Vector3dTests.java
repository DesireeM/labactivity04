//Desiree Mariano 2236344
package linearalgebra;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class Vector3dTests 
{
    /**
     * Rigorous Test :-)
     */

    @Test
    public void testGetMethods() {
        Vector3d vector = new Vector3d(1.0, 2.0, 3.0);
        assertEquals(1.0, vector.getX(), 0.001);
        assertEquals(2.0, vector.getY(), 0.001);
        assertEquals(3.0, vector.getZ(), 0.001);
    }

    @Test
    public void testMagnitude() {
        Vector3d vector = new Vector3d(1.0, 2.0, 2.0);
        assertEquals(3.0, vector.magnitude(), 0.001);
    }


    @Test
    public void testDotProduct() {
        Vector3d vector1 = new Vector3d(1.0, 1.0, 2.0);
        Vector3d vector2 = new Vector3d(2.0, 3.0, 4.0);
        assertEquals(13.0, vector1.dotProduct(vector2), 0.001);
    }

    @Test
    public void testAdd() {
        Vector3d vector1 = new Vector3d(1.0, 1.0, 2.0);
        Vector3d vector2 = new Vector3d(2.0, 3.0, 4.0);
        Vector3d sum = vector1.add(vector2);
        assertEquals(3.0, sum.getX(), 0.001);
        assertEquals(4.0, sum.getY(), 0.001);
        assertEquals(6.0, sum.getZ(), 0.001);
    }
}
