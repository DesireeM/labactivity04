//Desiree Mariano 2236344
package linearalgebra;

public class Vector3d {
    private double x;
    private double y;
    private double z;
    
    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX ()
    {
        return this.x;
    }

    public double getY ()
    {
        return this.y;
    }

    public double getZ ()
    {
        return this.z;
    }

    public double magnitude() {
        return Math.sqrt(Math.pow(x,2) + Math.pow(y,2) + Math.pow(z,2));
    }

    public double dotProduct(Vector3d v2) {
        return (this.x * v2.getX()) + (this.y * v2.getY()) + (this.z * v2.getZ());
    }

    public Vector3d add(Vector3d v2) {
        double newX = x + v2.getX();
        double newY = y + v2.getY();
        double newZ = z + v2.getZ();
        return new Vector3d(newX, newY, newZ);
    }

    public static void main(String[] args) {
        Vector3d vector1 = new Vector3d(1.0, 1.0, 2.0);
        Vector3d vector2 = new Vector3d(2.0, 3.0, 4.0);

        // Test the methods
        System.out.println("Vector1: (" + vector1.getX() + ", " + vector1.getY() + ", " + vector1.getZ() + ")");
        System.out.println("Vector2: (" + vector2.getX() + ", " + vector2.getY() + ", " + vector2.getZ() + ")");
        System.out.println("Magnitude: " + vector1.magnitude());
        System.out.println("Dot product: " + vector1.dotProduct(vector2));
        System.out.println(vector1.add(vector2));
       
    }

    public String toString() {
        return "(" + x + ", " + y + ", " + z + ")";
    }
}
